local CurrentPage = PageNames[props["page_index"].Value]

--[[layout["Code"] = {
  Style = "Text", TextBoxStyle = "Normal", 
  Position = {0,0}, Size = {0,0}
}]]

table.insert(graphics,{
  Type = "Label", Text = "USB Video Bridge Name",
  HTextAlign = "Left",
  VTextAlign = "Center",
  Position = {4,4}, Size = {130,16},
})

layout["named.bridge.component"] = {
  PrettyName = "USB Video Bridge Name",
  Style = "ComboBox",
  TextBoxStyle = "Normal",
  Position = {134,4},
  HTextAlign = "Center",
  Size = {120,16},
}

if CurrentPage == "Camera Control" then
  layout["current.camera"] = {
    PrettyName = "Current Camera",
    Style = "Indicator",
    TextBoxStyle = "NoBackground",
    Position = {204,4},
    FontStyle = "Bold",
    FontSize = 12,
    HTextAlign = "Right",
    Size = {232,16},
    FontColor = {0,0,0,0},
    StrokeWidth = 0
  }
  
  --PTZ Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Pan / Tilt / Zoom",
    StrokeWidth = 1,
    Position = {4,24},
    Size = {435,221},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Pan, Tilt, and Zoom Controls",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,44}, Size = {64,102}
  })

  layout["pan.left.tilt.up"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left / Tilt Up",
    Style = "Button",
    Position = {78,44},
    Size = {32,32},
  }

  layout["tilt.up"] = {
    PrettyName = "Pan-Tilt-Zoom~Tilt Up",
    Style = "Button",
    Position = {113,44},
    Size = {32,32},
  }
  
  layout["pan.right.tilt.up"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right / Tilt Up",
    Style = "Button",
    Position = {148,44},
    Size = {32,32},
  }

  layout["pan.left"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left",
    Style = "Button",
    Position = {78,79},
    Size = {32,32},
  }

  layout["pan.right"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right",
    Style = "Button",
    Position = {148,79},
    Size = {32,32},
  }

  layout["pan.left.tilt.down"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left / Tilt Down",
    Style = "Button",
    Position = {78,114},
    Size = {32,32},
  }

  layout["tilt.down"] = {
    PrettyName = "Pan-Tilt-Zoom~Tilt Down",
    Style = "Button",
    Position = {113,114},
    Size = {32,32},
  }

  layout["pan.right.tilt.down"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right / Tilt Down",
    Style = "Button",
    Position = {148,114},
    Size = {32,32},
    ZOrder
  }

  layout["zoom.in"] = {
    PrettyName = "Pan-Tilt-Zoom~Zoom In",
    Style = "Button",
    Position = {183,44},
    Size = {32,32},
  }

  layout["zoom.out"] = {
    PrettyName = "Pan-Tilt-Zoom~Zoom Out",
    Style = "Button",
    Position = {183,79},
    Size = {32,32},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Coordinates",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,149}, Size = {64,16}
  })

  layout["ptz.preset"] = {
    PrettyName = "Pan-Tilt-Zoom~Coordinates",
    Style = "TextBox",
    TextBoxStyle = "Normal",
    Position = {78,149},
    HTextAlign = "Center",
    Size = {102,16}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Speed By Zoom",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,168}, Size = {102,16}
  })

  layout["setup.speed.by.zoom"] = {
    PrettyName = "Pan-Tilt-Zoom~Speed By Zoom",
    Style = "Button",
    Position = {113,168},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Recalibrate PTZ",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,187}, Size = {102,16}
  })

  layout["ptz.recalibrate"] = {
    PrettyName = "Pan-Tilt-Zoom~Recalibrate PTZ",
    Style = "Button",
    Position = {113,187},
    Size = {32,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Ceiling Mount",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,206}, Size = {102,16}
  })

  layout["setup.ceiling.mount"] = {
    PrettyName = "Pan-Tilt-Zoom~Ceiling Mount",
    Style = "Button",
    Position = {113,204},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Is Moving",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,225}, Size = {102,16}
  })
  
  layout["is.moving"] = {
    PrettyName = "Pan-Tilt-Zoom~Is Moving",
    Style = "Indicator",
    Position = {121,225},
    Size = {16,16},
    Color = {0,159,60},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Home",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {218,52}, Size = {48,16}
  })

  layout["preset.home.load"] = {
    PrettyName = "Pan-Tilt-Zoom~Home",
    Style = "Button",
    Position = {269,44},
    Size = {32,32},
    Color = {0,159,60}
  }

  layout["preset.home.save.trigger"] = {
    PrettyName = "Pan-Tilt-Zoom~Save Home",
    Style = "Button",
    Position = {304,44},
    Size = {32,32},
    Color = {223,0,36},
    Legend = "Save\nHome"
  }

  table.insert(graphics,{
    Type = "Label", Text = "Privacy",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {218,87}, Size = {48,16}
  })  

  layout["toggle.privacy"] = {
    PrettyName = "Pan-Tilt-Zoom~Privacy",
    Style = "Button",
    Position = {269,79},
    Size = {32,32},
    UnlinkOffColor = true, 
    Color = {0,159,60}, 
    OffColor = {137,137,137}
  }

  layout["preset.privacy.save.trigger"] = {
    PrettyName = "Pan-Tilt-Zoom~Save Privacy",
    Style = "Button",
    Position = {304,79},
    Size = {32,32},
    Color = {223,0,36},
    Legend = "Save\nPrivacy"
  }

  layout["toggle.privacy.auto"] = {
    PrettyName = "Pan-Tilt-Zoom~Auto Privacy Mode",
    Style = "Button",
    Position = {374,79},
    Size = {32,32},
    Color = {0,159,60},
    Legend = "Auto\nPrivacy"
  }

  table.insert(graphics,{
    Type = "Label", Text = "AP Delay",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,130}, Size = {83,16}
  })  

  layout["setup.privacy.time"] = {
    PrettyName = "Pan-Tilt-Zoom~Auto Privacy Delay",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {269,130},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {166,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Pan Speed",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,149}, Size = {83,16}
  })  

  layout["setup.pan.speed"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Speed",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {269,149},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {166,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Tilt Speed",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,168}, Size = {83,16}
  })  

  layout["setup.tilt.speed"] = {
    PrettyName = "Pan-Tilt-Zoom~Tilt Speed",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {269,168},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {166,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Zoom Speed",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,187}, Size = {83,16}
  })

  layout["setup.zoom.speed"] = {
    PrettyName = "Pan-Tilt-Zoom~Zoom Speed",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {269,187},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {166,16},
  }
  
  table.insert(graphics,{
    Type = "Label", Text = "Recall Speed",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,206}, Size = {83,16}
  })  

  layout["aaaa.setup.snapshot.speed"] = {
    PrettyName = "Pan-Tilt-Zoom~Auto Privacy Delay",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {269,206},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {166,16},
  }

  --Focus Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Focus",
    StrokeWidth = 1,
    Position = {4,265},
    Size = {432,90},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Mode",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {75,281}, Size = {32,16}
  })  

  layout["focus.auto"] = {
    PrettyName = "Focus~AF",
    Style = "Button",
    Position = {78,300},
    Size = {32,32},
    Legend = "AF"
  }

  table.insert(graphics,{
    Type = "Label", Text = "In",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {113,281}, Size = {32,16}
  })  

  layout["focus.far"] = {
    PrettyName = "Focus~In",
    Style = "Button",
    Position = {113,300},
    Size = {32,32}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Out",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {148,281}, Size = {32,16}
  })  

  layout["focus.near"] = {
    PrettyName = "Focus~Out",
    Style = "Button",
    Position = {148,300},
    Size = {32,32}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Auto",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {78,335}, Size = {32,16}
  })  

  table.insert(graphics,{
    Type = "Label", Text = "Manual",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {113,335}, Size = {67,16}
  })  

  table.insert(graphics,{
    Type = "Label", Text = "Manual Speed",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,281}, Size = {80,16}
  })  

  layout["setup.focus.speed"] = {
    PrettyName = "Focus~Manual Speed",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {266,281},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {160,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Sensitivity",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {183,300}, Size = {80,16}
  })  

  layout["setup.focus.sensitivity"] = {
    PrettyName = "Focus~Sensitivity",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {266,300},
    HTextAlign = "Center",
    Size = {160,16},
    Choices = {"Test"}
  }

  --Preview Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Preview",
    StrokeWidth = 1,
    Position = {4,371},
    Size = {432,204},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  layout["pan.left.tilt.up.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left / Tilt Up",
    Style = "Button",
    Position = {107,391},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100008
  }

  layout["tilt.up.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Tilt Up",
    Style = "Button",
    Position = {251,391},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100007
  }
  
  layout["pan.right.tilt.up.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right / Tilt Up",
    Style = "Button",
    Position = {395,391},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100006
  }

  layout["pan.left.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left",
    Style = "Button",
    Position = {107,465},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100005
  }

  layout["pan.right.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right",
    Style = "Button",
    Position = {395,465},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100004
  }

  layout["pan.left.tilt.down.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Left / Tilt Down",
    Style = "Button",
    Position = {107,539},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100003
  }

  layout["tilt.down.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Tilt Down",
    Style = "Button",
    Position = {251,539},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100002
  }

  layout["pan.right.tilt.down.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Pan Right / Tilt Down",
    Style = "Button",
    Position = {395,539},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100001
  }

  layout["zoom.in.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Zoom In",
    Style = "Button",
    Position = {179,391},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100011
  }

  layout["zoom.out.preview"] = {
    PrettyName = "Pan-Tilt-Zoom~Zoom Out",
    Style = "Button",
    Position = {323,391},
    Size = {32,32},
    Color = {0,0,0,0},
    Margin = 5,
    ZOrder=100010
  }

  layout["preview"] = {
    PrettyName = "Preview",
    Style = "Media Display",
    Position = {107,391},
    HTextAlign = "Center",
    Color = {194,194,194},
    Size = {320,180},
    ZOrder=10000
  }

  table.insert(graphics,{
    Type = "Label", Text = "NOTE: Media Display is not currently supported in plugins. Drag Preview to Schematic to change presentation type to Media Display",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {107,425}, Size = {320,48},
    ZOrder=100009
  })  

elseif CurrentPage == "Camera Imaging" then
  layout["current.camera"] = {
    PrettyName = "Current Camera",
    Style = "Indicator",
    TextBoxStyle = "NoBackground",
    Position = {204,4},
    FontStyle = "Bold",
    FontSize = 12,
    HTextAlign = "Right",
    Size = {232,16},
    FontColor = {0,0,0,0},
    StrokeWidth = 0
  }

  table.insert(graphics,{
    Type = "Label", Text = "Reset all Camera Settings (Imaging, PTZ, and Focus)",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {4,24}, Size = {400,16},
  })  

  layout["setup.reset.settings"] = {
    PrettyName = "Imaging~Reset Cam Settings",
    Style = "Button",
    Position = {407,24},
    Size = {32,16},
  }

  --Imaging Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Imaging",
    StrokeWidth = 1,
    Position = {4,40},
    Size = {432,59},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Brightness",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,60}, Size = {64,16},
  })  
  
  layout["img.brightness"] = {
    PrettyName = "Imaging~Brightness",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {75,60},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Saturation",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,79}, Size = {64,16},
  })  

  layout["img.saturation"] = {
    PrettyName = "Imaging~Saturation",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {75,79},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Sharpness",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,60}, Size = {96,16},
  })  

  layout["img.sharpness"] = {
    PrettyName = "Imaging~Sharpness",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,60},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Contrast",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,79}, Size = {96,16},
  })  

  layout["img.contrast"] = {
    PrettyName = "Imaging~Contrast",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,79},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  --Exposure Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Exposure",
    StrokeWidth = 1,
    Position = {4,115},
    Size = {432,135},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Mode",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,135}, Size = {96,16},
  })  

  layout["exp.mode"] = {
    PrettyName = "Exposure~Mode",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {107,135},
    HTextAlign = "Center",
    Size = {96,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Backlight Comp",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,173}, Size = {96,16},
  })  

  layout["exp.backlight.comp"] = {
    PrettyName = "Exposure~Backlight Comp",
    Style = "Button",
    Position = {107,173},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Anti-Flicker",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,192}, Size = {96,16},
  })  

  layout["exp.anti.flicker"] = {
    PrettyName = "Exposure~Anti-Flicker",
    Style = "Button",
    Position = {107,192},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Compensation",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,135}, Size = {96,16},
  })  

  layout["exp.comp"] = {
    PrettyName = "Exposure~Compensation",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,135},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Gain Limit",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,154}, Size = {96,16},
  })  

  layout["exp.gain.limit"] = {
    PrettyName = "Exposure~Gain Limit",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,154},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Dynamic Range",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,173}, Size = {96,16},
  })  

  layout["exp.dynamic.range"] = {
    PrettyName = "Exposure~Dynamic Range",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,173},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Aperture",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,192}, Size = {96,16},
  })  

  layout["exp.iris"] = {
    PrettyName = "Exposure~Aperture",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {305,192},
    HTextAlign = "Center",
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Shutter",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,211}, Size = {96,16},
  })  

  layout["exp.shutter"] = {
    PrettyName = "Exposure~Aperture",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {305,211},
    HTextAlign = "Center",
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Gain",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,230}, Size = {96,16},
  })  

  layout["exp.gain"] = {
    PrettyName = "Exposure~Gain",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,230},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  --White Balance Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "White Balance",
    StrokeWidth = 1,
    Position = {4,266},
    Size = {432,116},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Mode",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,286}, Size = {96,16},
  })  

  layout["wb.awb.mode"] = {
    PrettyName = "White Balance~Mode",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {107,286},
    HTextAlign = "Center",
    Size = {96,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "AWB Sensitivity",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,306}, Size = {96,16},
  })  

  layout["wb.awb.sensitivity"] = {
    PrettyName = "White Balance~AWB Sesntitivity",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {107,305},
    HTextAlign = "Center",
    Size = {96,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "AWB One Push",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,326}, Size = {96,16},
  })  

  layout["wb.one.push.trigger"] = {
    PrettyName = "White Balance~AWB One Push",
    Style = "Button",
    Position = {107,324},
    Size = {32,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "AWB Sensitivity",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,306}, Size = {96,16},
  })  

  table.insert(graphics,{
    Type = "Label", Text = "Hue",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,286}, Size = {96,16},
  })  

  layout["wb.hue"] = {
    PrettyName = "White Balance~Hue",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,286},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Auto Red Gain",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,326}, Size = {96,16},
  })  

  layout["wb.awb.red.gain"] = {
    PrettyName = "White Balance~Auto Red Gain",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,305},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Auto Blue Gain",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,306}, Size = {96,16},
  })  

  layout["wb.awb.blue.gain"] = {
    PrettyName = "White Balance~Auto Blue Gain",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,324},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Manual Red Gain",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,343}, Size = {96,16},
  })  

  layout["wb.red.gain"] = {
    PrettyName = "White Balance~Manual Red Gain",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,343},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Manual Blue Gain",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,362}, Size = {96,16},
  })  

  layout["wb.blue.gain"] = {
    PrettyName = "White Balance~Manual Blue Gain",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,362},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  --Noise Reduction Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Noise Reduction",
    StrokeWidth = 1,
    Position = {4,398},
    Size = {432,78},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "2D NR Mode",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,418}, Size = {96,16},
  })  

  layout["setup.2d.nr.mode"] = {
    PrettyName = "Noise Reduction~2D NR Mode",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {107,418},
    HTextAlign = "Center",
    Size = {96,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "3D NR Enable",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,437}, Size = {96,16},
  })

  layout["setup.3d.nr.enable"] = {
    PrettyName = "Noise Reduction~3D NR Enable",
    Style = "Button",
    Position = {107,437},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "Hot Pixel Enable",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,456}, Size = {96,16},
  })  

  layout["setup.hot.pixel.enable"] = {
    PrettyName = "Noise Reduction~3D NR Enable",
    Style = "Button",
    Position = {107,456},
    Size = {32,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "2D NR",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,418}, Size = {96,16},
  })  

  layout["setup.2d.nr.value"] = {
    PrettyName = "Noise Reduction~2D NR",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,418},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "3D NR",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,437}, Size = {96,16},
  })  

  layout["setup.3d.nr.value"] = {
    PrettyName = "Noise Reduction~3D NR",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,437},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Hot Pixel",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {206,456}, Size = {96,16},
  })  

  layout["setup.hot.pixel.value"] = {
    PrettyName = "Noise Reduction~Hot Pixel",
    Style = "TextBox",
    TextBoxStyle = "Meter",
    Position = {305,456},
    HTextAlign = "Center",
    Color = {124,155,207},
    Size = {128,16},
  }

  --SDI/HDMI Format Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "SDI/HDMI Format",
    StrokeWidth = 1,
    Position = {4,492},
    Size = {432,116},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Imaging Frame Rate (Hz)",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,512}, Size = {150,16},
  })  

  layout["video.format.imaging.frame.rate"] = {
    PrettyName = "Video Format~Imaging Frame Rate",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {161,512},
    HTextAlign = "Center",
    Size = {150,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "SDI/HDMI Enable",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,531}, Size = {150,16},
  })

  layout["setup.hdmi.enable"] = {
    PrettyName = "Video Format~SDI/HDMI Enable",
    Style = "Button",
    Position = {161,532},
    Size = {40,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "SDI/HDMI Mode",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,550}, Size = {150,16},
  })  

  layout["setup.hdmi.mode"] = {
    PrettyName = "Video Format~SDI/HDMI Enable",
    Style = "Button",
    Position = {161,550},
    Size = {40,16},
    Color = {105,192,138}
  }

  table.insert(graphics,{
    Type = "Label", Text = "SDI/HDMI Format",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,569}, Size = {150,16},
  })  

  layout["setup.hdmi.output.format"] = {
    PrettyName = "Video Format~SDI/HDMI Format",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {161,569},
    HTextAlign = "Center",
    Size = {150,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "SDI Level A/B",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,588}, Size = {150,16},
  })  

  layout["setup.sdi.level"] = {
    PrettyName = "Video Format~SDI/HDMI Enable",
    Style = "Button",
    Position = {161,588},
    Size = {40,16},
    Color = {105,192,138}
  }


elseif CurrentPage == "Mediacast Streams" then
  --IP Streams Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "IP Streams",
    StrokeWidth = 1,
    Position = {4,24},
    Size = {436,135},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Preview Stream",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {111,44}, Size = {120,16},
  })  

  layout["ip.streams.preview.enable"] = {
    PrettyName = "IP Stream~Preview Stream~Enable",
    Style = "Button",
    Position = {231,44},
    Size = {40,16},
    Color = {105,192,138},
    Legend = "Enable"
  }

  table.insert(graphics,{
    Type = "Label", Text = "RTSP URL",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,63}, Size = {100,16},
  })  

  layout["ip.streams.preview.rtsp.url"] = {
    PrettyName = "IP Stream~Preview Stream~RSTP URL",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {111,63},
    HTextAlign = "Center",
    Size = {160,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "RTP Address",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,82}, Size = {100,16},
  })  

  layout["ip.streams.preview.rtp.address"] = {
    PrettyName = "IP Stream~Preview Stream~RTP Address",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {111,82},
    HTextAlign = "Center",
    Size = {160,16},
  }

  layout["ip.streams.lbr.rtp.address"] = {
    PrettyName = "IP Stream~Mediacast Stream~RTP Address",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {276,82},
    HTextAlign = "Center",
    Size = {160,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Streaming Mode",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,101}, Size = {100,16},
  })  

  table.insert(graphics,{
    Type = "Label", Text = "fps",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {247,101}, Size = {26,16},
  })  

  layout["ip.streams.preview.streaming.mode.fps"] = {
    PrettyName = "IP Stream~Preview Stream~Streaming Mode Frame Rate",
    Style = "TextBox",
    TextBoxStyle = "Normal",
    Position = {219,101},
    HTextAlign = "Center",
    Color = {255,255,255},
    Size = {26,16},
  }

  layout["ip.streams.preview.streaming.mode"] = {
    PrettyName = "IP Stream~Preview Stream~Streaming Mode",
    Style = "ComboBox",
    TextBoxStyle = "Normal",
    Position = {111,101},
    HTextAlign = "Center",
    Size = {106,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Video Format",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,120}, Size = {100,16},
  })  

  layout["ip.streams.preview.video.format"] = {
    PrettyName = "IP Stream~Preview Stream~Video Format",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {111,120},
    HTextAlign = "Center",
    Size = {160,16},
  }

  layout["ip.streams.lbr.video.format"] = {
    PrettyName = "IP Stream~Mediacast Stream~Video Format",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {276,120},
    HTextAlign = "Center",
    Size = {160,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Max Bitrate (Mbps)",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {8,139}, Size = {100,16},
  })  

  layout["ip.streams.preview.max.bitrate"] = {
    PrettyName = "IP Stream~Preview Stream~Max Bitrate",
    Style = "TextBox",
    TextBoxStyle = "Normal",
    Position = {111,139},
    HTextAlign = "Center",
    Color = {255,255,255},
    Size = {160,16},
  }

  layout["ip.streams.lbr.max.bitrate"] = {
    PrettyName = "IP Stream~Mediacast Stream~Max Bitrate",
    Style = "TextBox",
    TextBoxStyle = "Normal",
    Position = {276,139},
    HTextAlign = "Center",
    Color = {255,255,255},
    Size = {160,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Medicast Stream",
    HTextAlign = "Center",
    VTextAlign = "Center",
    Position = {276,44}, Size = {160,16},
  })  


elseif CurrentPage == "Status" then
  --IP Streams Section
  table.insert(graphics,{
    Type = "GroupBox",
    Text = "Video",
    StrokeWidth = 1,
    Position = {4,24},
    Size = {388, 220},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "GroupBox",
    StrokeWidth = 1,
    StrokeColor = {137,137,137},
    Position = {11,44},
    Size = {190, 168},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  --Input Section
  table.insert(graphics,{
    Type = "Label", Text = "Input",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {11,44}, Size = {92,16},
    FontStyle = "Bold",
  })  

  layout["peripheral.active.network"] = {
    PrettyName = "Status~Input Active",
    Style = "Indicator",
    Position = {103,44},
    Size = {16,16},
    Color = {0,255,0},
  }

  layout["peripheral.camera.source"] = {
    PrettyName = "Status~Input Connection",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {11,63},
    HTextAlign = "Center",
    Size = {190,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Bitrate (Kb/s)",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,82}, Size = {100,16},
  })  

  layout["peripheral.stream1.stats.avg.kbps"] = {
    PrettyName = "Status~IP Stream 1~Bitrate",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,82},
    HTextAlign = "Center",
    Size = {87,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "FPS",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,101}, Size = {100,16},
  })  

  layout["peripheral.stream1.stats.instant.fps"] = {
    PrettyName = "Status~IP Stream 1~FPS",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,101},
    HTextAlign = "Center",
    Size = {87,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Frame Count",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,121}, Size = {100,16},
  })  

  layout["peripheral.stream1.frame.count.number"] = {
    PrettyName = "Status~IP Stream 1~Frame Count",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,120},
    HTextAlign = "Center",
    Size = {87,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Packet Count",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,139}, Size = {92,16},
  })  

  layout["peripheral.stream1.packet.count.number"] = {
    PrettyName = "Status~IP Stream 1~Packet Count",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,139},
    HTextAlign = "Center",
    Size = {87,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Packets Lost",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,158}, Size = {92,16},
  })  

  layout["peripheral.stream1.packet.loss.number"] = {
    PrettyName = "Status~IP Stream 1~Packets Lost",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,158},
    HTextAlign = "Center",
    Size = {87,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Packet Loss %",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {11,177}, Size = {92,16},
  })  

  layout["peripheral.stream1.packet.loss.percent"] = {
    PrettyName = "Status~IP Stream 1~Packet Loss Percentage",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {114,177},
    HTextAlign = "Center",
    Size = {87,16},
  }

  --USB Section
  table.insert(graphics,{
    Type = "GroupBox",
    StrokeWidth = 1,
    StrokeColor = {137,137,137},
    Position = {204,44},
    Size = {184, 168},
    CornerRadius = 8,
    HTextAlign = "Left",
  })

  table.insert(graphics,{
    Type = "Label", Text = "Video Format",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,82}, Size = {100,16},
  })  

  table.insert(graphics,{
    Type = "Label", Text = "USB",
    HTextAlign = "Right",
    VTextAlign = "Center",
    Position = {204,44}, Size = {92,16},
    FontStyle = "Bold",
  })

  layout["peripheral.active.usb"] = {
    PrettyName = "Status~USB~Active",
    Style = "Indicator",
    Position = {296,44},
    Size = {16,16},
    Color = {0,255,0},
  }

  layout["peripheral.bridge.name"] = {
    PrettyName = "Status~USB~Bridge Name",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {204,63},
    HTextAlign = "Center",
    Size = {184,16},
  }

  layout["peripheral.usb.video.format"] = {
    PrettyName = "Status~USB~Video Format",
    Style = "Indicator",
    Position = {307,82},
    Size = {81,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Encoding",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,101}, Size = {100,16},
  })  

  layout["peripheral.stats.usb.encoding"] = {
    PrettyName = "Status~USB~Encoding",
    Style = "Indicator",
    Position = {307,101},
    Size = {81,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "USB Speed",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,120}, Size = {100,16},
  })

  layout["peripheral.stats.usb.speed"] = {
    PrettyName = "Status~USB~USB Speed",
    Style = "Indicator",
    Position = {307,120},
    Size = {81,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "Bitrate (Mb/s)",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,139}, Size = {92,16},
  })  

  layout["peripheral.stats.usb.avg.mbps"] = {
    PrettyName = "Status~USB~Bitrate",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {307,139},
    HTextAlign = "Center",
    Size = {81,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "FPS",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,158}, Size = {92,16},
  })  

  layout["peripheral.stats.usb.fps"] = {
    PrettyName = "Status~USB~FPS",
    Style = "Indicator",
    TextBoxStyle = "Normal",
    Position = {307,158},
    HTextAlign = "Center",
    Size = {81,16},
  }


  table.insert(graphics,{
    Type = "Label", Text = "SRC-",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,177}, Size = {92,16},
  })  

  layout["peripheral.usb.src.minus"] = {
    PrettyName = "Status~USB~SRC-",
    Style = "Indicator",
    Position = {307,177},
    Size = {81,16},
  }

  table.insert(graphics,{
    Type = "Label", Text = "SRC+",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,196}, Size = {92,16},
  })  

  layout["peripheral.usb.src.plus"] = {
    PrettyName = "Status~USB~SRC+",
    Style = "Indicator",
    Position = {307,196},
    Size = {81,16},
  }

  layout["peripheral.stream1.test"] = {
    PrettyName = "Status~IP Stream 1~Test Stream 1",
    Style = "Button",
    Position = {24,196},
    Size = {74,16},
    Legend = "Test"
  }

  table.insert(graphics,{
    Type = "Label", Text = "Reset",
    HTextAlign = "Left",
    VTextAlign = "Center",
    Position = {204,224}, Size = {100,16},
  })  

  layout["peripheral.stats.reset"] = {
    PrettyName = "Status~Reset",
    Style = "Button",
    Position = {307,224},
    Size = {81,16},
  }
end