
function hasProperty(ctl, prop)
  if prop == "IsDisabled" then
    return pcall(function() local tmp = ctl.IsDisabled end)
  elseif prop == "Choices" then
    return pcall(function() local tmp = ctl.Choices end)
  elseif prop == "Legend" then
    return pcall(function() local tmp = ctl.Legend end)
  elseif prop == "IsIndeterminate" then
    return pcall(function() local tmp = ctl.IsIndeterminate end)
  end
end

function updateDisableds()
  for c,ctl in pairs (Controls) do
    if hasProperty(COMP[c], "IsDisabled") and not string.find(c, ".preview") then
      ctl.IsDisabled = COMP[c].IsDisabled
    end
  end
end

function updateSDIHDMIControls(name)
  if name == "setup.hdmi.output.format" then
    Controls["setup.hdmi.output.format"].Choices = COMP["setup.hdmi.output.format"].Choices
  end
end

function updateChoices()
  for c,ctl in pairs (Controls) do
    if hasProperty(COMP[c], "Choices") and not string.find(c, ".preview") then
      ctl.Choices = COMP[c].Choices
    end
  end
end

function updateLegends()
  for c,ctl in pairs (Controls) do
    if hasProperty(COMP[c], "Legend") and not string.find(c, ".preview") then
      ctl.Legend = COMP[c].Legend
    end
  end
end

function updateInd()
  for c,ctl in pairs (Controls) do
    if hasProperty(COMP[c], "IsIndeterminate") and not string.find(c, "preview") then
      ctl.IsIndeterminate = COMP[c].IsIndeterminate
    end
    if not hasProperty(COMP[c], "IsIndeterminate") then
      ctl.IsIndeterminate = false
    end
  end
  for c,ctl in pairs (Controls) do
    if string.find(c, ".preview") then
      ctl.IsIndeterminate = COMP["pan.left.tilt.up"].IsIndeterminate
    end
  end
end

function watchInd()
  if validChoice() then
    if hasProperty(COMP["pan.left.tilt.up"], "IsIndeterminate") then
      cam_state = COMP["pan.left.tilt.up"].IsIndeterminate 
    end
    if hasProperty(COMP["peripheral.bridge.name"], "IsIndeterminate") then
      bridge_state = COMP["peripheral.bridge.name"].IsIndeterminate
    end
    
    if cam_state ~= last_cam_state then
      updateInd()
      last_cam_state = cam_state
    end
    if bridge_state ~= last_bridge_state then
      updateInd()
      last_bridge_state = bridge_state
    end
  end
end

UpdateIndTimer = Timer.New()
UpdateIndTimer.EventHandler = watchInd
UpdateIndTimer:Start(.5)

function handleControl(ctl)
  for k,v in pairs (virtual_controls) do
    if ctl == v[2] then
      Controls[v[3]].String = v[2].String
      updateSDIHDMIControls(v[3])
      updateDisableds()
      updateLegends()
    end
    if ctl.Index == v[1] then
      v[2].Value = ctl.Value
      v[2].String = ctl.String
      v[2]:Trigger()
      updateSDIHDMIControls(v[3])
      updateDisableds()
      updateLegends()
    end
  end
end

function assignControls(component)
  virtual_controls = {}
  COMP = component
  cam_state = ""
  bridge_state = ""
  
  watchInd()
  for name, control in pairs (COMP) do
    if Controls[name] then
      --print(name, control)
      control.EventHandler = handleControl
      table.insert(virtual_controls, {Controls[name].Index, control, tostring(name)}) 
      Controls[name].String = control.String
      updateChoices()
    end
  end
  updateDisableds()
  --assign preview links
  parent = "pan.left.tilt.up"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "tilt.up"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "pan.right.tilt.up"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "pan.left"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "pan.right"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "pan.left.tilt.down"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "tilt.down"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "pan.right.tilt.down"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "zoom.in"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
  parent = "zoom.out"
  table.insert(virtual_controls, {Controls[parent..".preview"].Index, COMP[parent], tostring(parent..".preview")}) 
end

function updateNamedComponent()
  if Controls["named.bridge.component"].String ~= "" and validChoice() then
    Bridge = Component.New(Controls["named.bridge.component"].String)
    assignControls(Bridge)
  else
    Controls["named.bridge.component"].String = ""
  end
end


named_bridges = {}
for k,v in pairs (Component.GetComponents()) do
  for kk, vv in pairs(v) do
    if vv == "usb_uvc" then
      table.insert(named_bridges, v["ID"])
    end
  end
end

function validChoice()
  name_exists = false
  for k,v in pairs (named_bridges) do
    if v == Controls["named.bridge.component"].String then
      name_exists = true
    end
  end
  return name_exists
end

Controls["named.bridge.component"].Choices = named_bridges

updateNamedComponent()
for c, ctl in pairs (Controls) do
  ctl.EventHandler = handleControl
end
Controls["named.bridge.component"].EventHandler = updateNamedComponent