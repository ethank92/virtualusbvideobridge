table.insert(ctrls,{
  Name = "named.bridge.component", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "current.camera",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "None",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.left.tilt.up",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left-Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "tilt.up",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right.tilt.up",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right-Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.left",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.left.tilt.down",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left-Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "tilt.down",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right.tilt.down",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right-Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "zoom.in",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Zoom In",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "zoom.out",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Zoom Out",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ptz.preset",
  ControlType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.speed.by.zoom",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ptz.recalibrate",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.ceiling.mount",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "is.moving",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "preset.home.load",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  Icon = "Home",
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "preset.home.save.trigger",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "preset.privacy.save.trigger",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "toggle.privacy",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  Icon = "Prohibited",
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "toggle.privacy.auto",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.privacy.time",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 600,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.pan.speed",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Float",
  Min = .001, Max = 1, 
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.tilt.speed",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Float",
  Min = .001, Max = 1, 
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.zoom.speed",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Float",
  Min = .001, Max = 1, 
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "aaaa.setup.snapshot.speed",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Float",
  Min = .001, Max = 1, 
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "focus.auto",
  ControlType = "Button",
  ButtonType = "Momentary",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "focus.far",
  ControlType = "Button",
  ButtonType = "Momentary",
  Count = 1,
  Icon = "Plus",
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "focus.near",
  ControlType = "Button",
  ButtonType = "Momentary",
  Count = 1,
  Icon = "Minus",
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.focus.speed",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Float",
  Min = .001, Max = 1, 
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls,{
  Name = "setup.focus.sensitivity", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "preview", 
  ControlType = "Text",
  PinStyle = "None", UserPin = true,
  Count = 1
})

table.insert(ctrls, {
  Name = "pan.left.tilt.up.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left-Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "tilt.up.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right.tilt.up.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right-Up",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.left.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.left.tilt.down.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Left-Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "tilt.down.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "pan.right.tilt.down.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Arrow Right-Down",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "zoom.in.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Zoom In",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "zoom.out.preview",
  ControlType = "Button",
  ButtonType = "Momentary",
  Icon = "Zoom Out",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

--Imaging
table.insert(ctrls, {
  Name = "setup.reset.settings",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "img.brightness",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 14,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "img.saturation",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 14,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "img.sharpness",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 14,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "img.contrast",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 14,  
  PinStyle = "Both",
  UserPin = true
})

--Exposure
table.insert(ctrls,{
  Name = "exp.mode", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "exp.shutter", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "exp.iris", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "exp.comp",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = -7, Max = 7,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "exp.dynamic.range",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 8,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "exp.gain.limit",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 15,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "exp.gain",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 7,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "exp.backlight.comp",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "exp.anti.flicker",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

--White Balance
table.insert(ctrls,{
  Name = "wb.awb.mode", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls,{
  Name = "wb.awb.sensitivity", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "wb.one.push.trigger",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "wb.hue",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 0, Max = 14,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "wb.awb.red.gain",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 255,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "wb.awb.blue.gain",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 255,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "wb.red.gain",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 5000,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "wb.blue.gain",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 5000,  
  PinStyle = "Both",
  UserPin = true
})

--Noise Reduction
table.insert(ctrls,{
  Name = "setup.2d.nr.mode", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "setup.3d.nr.enable",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.hot.pixel.enable",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.2d.nr.value",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 5,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.3d.nr.value",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 11,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.hot.pixel.value",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 5,  
  PinStyle = "Both",
  UserPin = true
})

--SDI/HDMI Format 
table.insert(ctrls,{
  Name = "video.format.imaging.frame.rate", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "setup.hdmi.enable",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "setup.hdmi.mode",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls,{
  Name = "setup.hdmi.output.format", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "setup.sdi.level",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

--Mediacast Streams
table.insert(ctrls, {
  Name = "ip.streams.preview.enable",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.preview.rtsp.url",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.preview.rtp.address",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls,{
  Name = "ip.streams.preview.streaming.mode", 
  ControlType = "Text", TextBoxType = "ComboBox",
  PinStyle = "Input", UserPin = true, 
  Count = 1
})

table.insert(ctrls, {
  Name = "ip.streams.preview.streaming.mode.fps",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 30,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.lbr.rtp.address",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.lbr.video.format",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.preview.video.format",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.preview.max.bitrate",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 2,  
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "ip.streams.lbr.max.bitrate",
  ControlType = "Knob",
  Count = 1,
  ControlUnit = "Integer",
  Min = 1, Max = 30,  
  PinStyle = "Both",
  UserPin = true
})

--Status
table.insert(ctrls, {
  Name = "peripheral.active.network",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.camera.source",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.active.usb",
  ControlType = "Indicator",
  IndicatorType = "LED",
  Count = 1,
  PinStyle = "Output",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.bridge.name",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.stats.avg.kbps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.stats.instant.fps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.frame.count.number",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.packet.count.number",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.packet.loss.number",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.packet.loss.percent",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stream1.test",
  ControlType = "Button",
  ButtonType = "Toggle",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

--

table.insert(ctrls, {
  Name = "peripheral.usb.video.format",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stats.usb.encoding",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stats.usb.speed",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stats.usb.avg.mbps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stats.usb.fps",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.usb.src.minus",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.usb.src.plus",
  ControlType = "Indicator",
  IndicatorType = "Text",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})

table.insert(ctrls, {
  Name = "peripheral.stats.reset",
  ControlType = "Button",
  ButtonType = "Trigger",
  Count = 1,
  PinStyle = "Both",
  UserPin = true
})


